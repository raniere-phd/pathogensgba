---
title: Team
type: team
image: /img/team-jumbotron.jpg
main:
  advisor:
    - name: Olivier Sparagano
    - name: Ladan Jahangiri
  chair:
    - name: Raniere Silva
    - name: Esa Karalliu
  session1:
    - name: Belete Haile Nega
    - name: F M Yasir Hasib
    - name: Sabir Hussain
    - name: Syed Saad Ul Hassan Bukhari
  session2:
    - name: Baolin Song
    - name: Esa Karalliu
    - name: Maryam Fanian
    - name: Muhammad Umair Aziz
    - name: Muhammad Zeeshan
  session3:
    - name: Bruno Castellaro
    - name: Jun Liu
    - name: Junsheng He
    - name: Raniere Silva
---


