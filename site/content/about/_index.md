---
title: About
type: about
image: /img/home-jumbotron.jpg
---

## Symposium on Ectoparasites and Zoonotic Vector-Borne Pathogens in the Guangdong-Hong Kong-Macau Greater Bay Area {class="f3 b lh-title mb2"}

The objective of this symposium is
to present research findings
related to ectoparasites and zoonotic vector-borne pathogens
and
facilitate universities in the Guangdong-Hong Kong-Macau Greater Bay Area
to explore collaborations
including
joint Mainland-Hong Kong-Macau grant applications,
future joint publications,
or
joint PhD students' supervision.

## Event Organised By {class="f3 b lh-title mb2"}

![](/img/cityu.jpg)

## Event Supported By {class="f3 b lh-title mb2"}

- The Guangdong-Hong Kong-Macau University Alliance Symposiums on Greater Bay Area Fund

## Event Sponsored By {class="f3 b lh-title mb2"}

![](/img/Diversity_Logo.jpg)
