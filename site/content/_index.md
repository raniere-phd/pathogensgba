---
title: "Symposium on Ectoparasites and Zoonotic Vector-Borne Pathogens in the Guangdong-Hong Kong-Macau Greater Bay Area"
image: /img/home-jumbotron.jpg
blurb:
    heading: 05 April 2022
    text: "The symposium will show case the relevant research on Ectoparasites and Zoonotic Vector-Borne Pathogens in the Guangdong-Hong Kong-Macau Greater Bay Area."
intro:
    heading: "What we offer"
    text: "PathogensGBA will be an online afternoon of talks and panels covering"
products:
    - image: img/tick.svg
      text: "Ectoparasites zoonotic vector-borne pathogens."
    - image: /img/mosquito.svg
      text: "Other zoonotic vector-borne pathogens."
    - image: /img/lock.svg
      text: "FAIR Data applied to zoonotic vector-borne pathogens."
---

