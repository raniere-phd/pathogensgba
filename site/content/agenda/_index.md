---
title: Agenda for 05 April 2022
type: agenda
image: /img/agenda-jumbotron.jpg
main:
  programme:
    - time: 12:00
      title: Welcome session
    - time: 12:15
      title: Keynote session
      speaker: Dr Scott Edmunds
      job: Editor-in-Chief of GigaScience Press
      youtube: https://www.youtube.com/watch?v=sT9S8mvoeNA&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=2
    - time: 13:15
      title: Lunch break
    - time: 14:00
      title: Ectoparasites zoonotic vector-borne pathogens
    - time: 15:00
      title: Break
    - time: 15:10
      title: Other zoonotic vector-borne pathogens
    - time: 16:10
      title: Break
    - time: 16:20
      title: FAIR Data applied to zoonotic vector-borne pathogens
    - time: 17:00
      title: Break
    - time: 17:10
      title: Close announcements
    - time: 17:30
      title: Break up rooms available for small groups
    - time: 18:00
      title: Zoom room is close
  session1:
    - time: 14:00
      title: "A review on Diversity and Distribution of zoonotic Babesiosis and Theileriosis, their vector in Ruminants from Asia"
      speaker: Sabir Hussain
      job: PhD Candidate at City University of Hong Kong
      youtube: https://www.youtube.com/watch?v=3PmWT4CA-vs&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=3
    - time: 14:10
      title: Technology and application for Aedes mosquito control based on SIT
      speaker: Professor Zhongdao Wu
      job: "Director at Key Laboratory of Tropical Disease Control (Sun Yat-sen University), Ministry of Education, China" 
      youtube: https://www.youtube.com/watch?v=n6Qq1JRGRHE&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=4
    - time: 14:20
      title: Living with tick parasites in the MENA region
      speaker: Dr Nighat Perveen
      job: United Arab Emirates University
      youtube: https://www.youtube.com/watch?v=w3oFwykidzk&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=5
    - time: 14:30
      title: "Sand Tampan: Issues and options"
      speaker: Professor Abdullah Arijo
      job: Sindh Agriculture University, Tandojam, Pakistan
      youtube: https://www.youtube.com/watch?v=h3vbmOxWUQQ&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=6
    - time: 14:40
      title: Panel
      youtube: https://www.youtube.com/watch?v=ttzi_vjLlZk&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=7
    - time: 15:00
      title: End of session
  panel1:
    - name: Professor Zhongdao Wu
    - name: Professor Abdullah Arijo
    - name: Dr Nighat Perveen
    - name: Sabir Hussain
  session2:
    - time: 15:10
      title: "Canine Ehrlichiosis: A neglected vector borne zoonotic disease in South and East Asia"
      speaker: Muhammad Umair Aziz
      job: PhD Candidate at City University of Hong Kong
      youtube: https://www.youtube.com/watch?v=EOckWm04kCo&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=8
    - time: 15:20
      title: "Status of ticks and tick borne diseases of canine in Pakistan"
      speaker: Dr Abdullah Saghir Ahmad
      job: Cholistan University of Veterinary and Animal Sciences Bahawalpur, Pakistan
      youtube: https://www.youtube.com/watch?v=vwMTEIyyzkE&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=9
    - time: 15:30
      title: "Molecular evidence of Anaplasma infection in naturally affected domestic cats of Pakistan"
      speaker: Dr Arslan Ahmed
      job: Department of Veterinary Medicine, University of Veterinary and Animal Sciences, Lahore
      youtube: https://www.youtube.com/watch?v=6aF_D32SKAM&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=10
    - time: 15:40
      title: Panel
      youtube: https://www.youtube.com/watch?v=dTV_N5nsdfY&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=11
    - time: 16:10
      title: End of session
  panel2:
    - name: Professor Abdullah Saghir Ahmad
    - name: Dr Arslan Ahmed
    - name: Muhammad Umair Aziz
  session3:
    - time: 16:20
      title: "Zoonotic diseases and FAIR data: a review of FAIR data used in China" 
      speaker: Raniere Silva
      job: PhD Candidate at City University of Hong Kong
      youtube: https://www.youtube.com/watch?v=g5cvwlKSnxY&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=12
    - time: 16:30
      title: "Why to publish in open platforms"
      speaker: Dr Sara Savić
      job: Researcher at the Scientific Veterinary Institute 'Novi Sad'
      youtube: https://www.youtube.com/watch?v=naUmwvpAYvE&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=13
    - time: 16:40
      title: Panel
      youtube: https://www.youtube.com/watch?v=Znf746PulU8&list=PLxvP2PvfiiWUi67IV09vSdxaOlMa2e4k7&index=14
    - time: 17:00
      title: End of session
  panel3:
    - name: Dr Scott Edmunds
    - name: Dr Sara Savić
    - name: Raniere Silva
---


